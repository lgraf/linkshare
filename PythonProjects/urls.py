from django.conf.urls import patterns, include, url
from django.contrib import admin
import linkshare.urls

admin.autodiscover()

urlpatterns = [
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^', include(linkshare.urls, namespace='linkshare')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^upload/', linkshare.urls.upload),
    url(r'^getLinks/$', linkshare.urls.getLinks),
    url(r'^url/(\w*)$', linkshare.urls.shorturl),
    url(r'^getTitle/$', linkshare.urls.getPagetitle),
]
