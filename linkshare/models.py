from django.db import models

class Link(models.Model):
    id = models.TextField(unique=True, primary_key=True, max_length=50)
    url = models.TextField(max_length=250)
    title = models.TextField(max_length=50)
    description = models.TextField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)
