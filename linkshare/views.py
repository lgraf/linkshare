from django.shortcuts import render_to_response, get_object_or_404, render
from django.template import RequestContext
from django.views.decorators.csrf import ensure_csrf_cookie
from models import *
import datetime
from django.http.response import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from bs4 import BeautifulSoup
import urllib2
import math

@ensure_csrf_cookie
@login_required
def mainpage(request):
    if request.method == 'POST':
        return
    items_per_page = 5
    page = 1
    try:
        page = int(request.GET.get('page', 1))
    except ValueError:
        page = 1
    pagelen = getPagelen(items_per_page)
    if page > pagelen:
        page = 1
    links = getLink(items_per_page, page)
    #pagelen = getPagelen(items_per_page)
    #if len(links) >= items_per_page or page > 1:
        #pagelen = getPagelen(items_per_page)
    pagination = getPagination(page, pagelen)
    return render(request, 'mainpage.html', {'rooturi': request.build_absolute_uri("/"), 'links': links, 'pagelen': pagelen, 'pagination': pagination})


@login_required
def upload(request):
    if request.method == 'POST':
        link = Link(id=request.POST['id'], url=request.POST['url'], title=request.POST['title'], description=request.POST['description'])
        link.save()
        return HttpResponse('success', content_type="text/plain")


@login_required
def getLinks(request):
    if request.method == 'POST':
        #timestamp=request.POST['timestamp']
        #newer=request.POST['newer']
        number = request.POST['number']
        page = request.POST['page']
        return getLink(number, page)


def getLink(count, page):
    startitem = count*(page - 1)
    links = Link.objects.all().order_by('-timestamp')[startitem:(startitem + count)]
    return links


def getPagelen(items_per_page):
    linkcount = Link.objects.count()
    return int(math.ceil(float(linkcount)/float(items_per_page)))


def oldgetLink(stamp, newr, count, page):
    startitem = count*(page - 1)
    if newr:
        links = Link.objects.filter(timestamp__gte=stamp).order_by('-timestamp')[startitem:count]
    else:
        links = Link.objects.filter(timestamp__lte=stamp).order_by('-timestamp')[startitem:count]
    return links


def shorturl(request, shorturl):
    url = get_object_or_404(Link, id=shorturl)
    return HttpResponseRedirect(url.url)


@login_required
def getPagetitle(request):
    if request.method == 'POST':
        soup = BeautifulSoup(urllib2.urlopen(request.POST['url']))
        return HttpResponse(soup.title.string, content_type="text/plain")


def getPagination(page, pagelen):
    itemsno = 11
    pagination = list()
    if (page == 1):
        pagination.append({"inner": '<i class="left arrow icon"></i>', "class": 'disabled icon item'})
    else:
        pagination.append({"inner": '<i class="left arrow icon"></i>', "class": 'icon item', "link": str(page - 1)})
    if (pagelen <= itemsno-2):
        for i in range(1, pagelen + 1):
            if(page == i):
                pagination.append({"inner": str(i), "class": 'active item'})
            else:
                pagination.append({"inner": str(i), "class": 'item', "link": str(i)})
    else:
        return

    if (page == pagelen):
        pagination.append({"inner": '<i class="right arrow icon"></i>', "class": 'disabled icon item'})
    else:
        pagination.append({"inner": '<i class="right arrow icon"></i>', "class": 'icon item', "link": str(page + 1)})

    return pagination
