# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linkshare', '0002_auto_20150112_1845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='id',
            field=models.TextField(max_length=50, unique=True, serialize=False, primary_key=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='url',
            field=models.TextField(max_length=250),
            preserve_default=True,
        ),
    ]
