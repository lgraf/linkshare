# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.TextField(max_length=50, unique=True, serialize=False, primary_key=True)),
                ('url', models.TextField(max_length=50)),
                ('title', models.TextField(max_length=50)),
                ('description', models.TextField(max_length=50)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
