from django.conf.urls import patterns, include, url
from views import *

urlpatterns = [
    url(r'^$', mainpage, name='mainpage'),
    url(r'^upload/$', upload),
]